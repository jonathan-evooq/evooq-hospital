package com.evooq.hospital.service

import com.evooq.hospital.model.*

class Simulator {

  fun simulate(patients: List<Patient>, drugs: List<Drug>): List<Patient> {
    return patients.map { patient -> patient administer drugs }
  }

  private infix fun Patient.administer(drugs: List<Drug>): Patient {
    return this
      .let { drugs.fold(it) { curr, drug -> drug.applyTo(curr) } } // <- apply drugs normally
      .let { if (it.currentState == State.DIABETES && Insulin !in drugs) it.copy(state = State.DEAD) else it } // <- diabetic without insulin will die
      .let { Miracle.applyTo(it) } // <- a sprinkle of miracle
  }
}