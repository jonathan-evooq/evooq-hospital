package com.evooq.hospital.model

import kotlin.random.Random
import kotlin.random.nextInt

sealed class Drug {
  abstract fun applyTo(patient: Patient): Patient
  override fun toString(): String = this::class.simpleName!!

  companion object {
    fun of(shorthand: String): Drug {
      return when(shorthand) {
        "As" -> Aspirin
        "An" -> Antibiotic
        "I"  -> Insulin
        "P"  -> Paracetamol
        else -> throw IllegalArgumentException("Unknown drug with shorthand: $shorthand")
      }
    }
  }
}

object Aspirin: Drug() {
  override fun applyTo(patient: Patient): Patient {
    return patient
      .let { if (it.currentState == State.FEVER) it.copy(state = State.HEALTHY) else it }
      .let { if (Paracetamol in it.history) it.copy(state = State.DEAD) else it }
      .let { it.copy(history = it.history + this) }
  }
}

object Antibiotic: Drug() {
  override fun applyTo(patient: Patient): Patient {
    return patient
      .let { if (it.currentState == State.TUBERCULOSIS) it.copy(state = State.HEALTHY) else it }
      .let { if (Insulin in it.history && it.currentState == State.HEALTHY) it.copy(state = State.FEVER) else it }
      .let { it.copy(history = it.history + this) }
  }
}

object Insulin: Drug() {
  override fun applyTo(patient: Patient): Patient {
    return patient
      .let { if (it.initialState == State.DIABETES && it.state == State.DEAD) it.copy(state = it.initialState) else it } // <- necromancy!
      .let { if (Antibiotic in it.history && it.currentState == State.HEALTHY) it.copy(state = State.FEVER) else it }
      .let { it.copy(history = it.history + this) }
  }
}

object Paracetamol: Drug() {
  override fun applyTo(patient: Patient): Patient {
    return patient
      .let { if (it.currentState == State.FEVER) it.copy(state = State.HEALTHY) else it }
      .let { if (Aspirin in it.history) it.copy(state = State.DEAD) else it }
      .let { it.copy(history = it.history + this) }
  }
}

/**
 * This is the Flying Spaghetti Monster, the name was just too long for my taste.
 * This Drug is slightly different than other in that it won't be recorded when you're not one-in-a-million.
 */
object Miracle: Drug() {
  override fun applyTo(patient: Patient): Patient {
    return when (Random.nextInt(1..1_000_000) == 1 && patient.currentState == State.DEAD) { // <- one in a million!
      true -> patient.copy(history = patient.history + this, state = State.HEALTHY)
      else -> patient //no miracle for you
    }
  }
}