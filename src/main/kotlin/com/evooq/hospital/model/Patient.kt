package com.evooq.hospital.model

data class Patient(
  val initialState: State,
  val history: List<Drug> = mutableListOf(),
  val state: State? = null
) {
  val currentState: State = state ?: initialState
}
