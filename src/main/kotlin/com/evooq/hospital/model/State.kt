package com.evooq.hospital.model

enum class State(val shorthand: String) {
  FEVER("F"),
  HEALTHY("H"),
  DIABETES("D"),
  TUBERCULOSIS("T"),
  DEAD("X"),
  ;

  companion object {
    fun of(shorthand: String): State = values()
      .find { it.shorthand == shorthand }
      ?: throw IllegalArgumentException("Unknown State with shorthand: $shorthand")
  }
}