package com.evooq.hospital

import com.evooq.hospital.model.Drug
import com.evooq.hospital.model.Patient
import com.evooq.hospital.model.State
import com.evooq.hospital.service.Simulator

class Application(private val simulator: Simulator) {
  fun run(patientsString: String, drugsString: String): String {
    val patients = patientsString
      .split(",")
      .map { it.toUpperCase() }
      .map { State.of(it) }
      .map { Patient(it, mutableListOf(), null) }

    val drugs = drugsString
      .split(",")
      .filter { it != "" }
      .map { Drug.of(it) }

    val simulated = simulator.simulate(patients, drugs)

    return State.values()
      .map { state -> state to simulated.count { it.currentState == state } }
      .joinToString(",") { (state, count) -> "${state.shorthand}:${count}" }
  }
}

fun main(args: Array<String>) {
  val simulator = Simulator()
  val application = Application(simulator)

  application
    .run(
      runCatching { args[0] }.getOrElse { throw it },
      runCatching { args[1] }.getOrDefault("")
    )
    .also { println(it) }
}