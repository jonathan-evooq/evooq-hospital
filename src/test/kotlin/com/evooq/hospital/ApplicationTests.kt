package com.evooq.hospital

import com.evooq.hospital.model.Drug
import com.evooq.hospital.model.Insulin
import com.evooq.hospital.model.Patient
import com.evooq.hospital.model.State
import com.evooq.hospital.service.Simulator
import io.kotest.matchers.nulls.beNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class ApplicationTests {

  private val simulator = mockk<Simulator>()
  private val application = Application(simulator)

  @Nested
  inner class GivenOnlyPatientsArgument {

    @Nested
    inner class WhenCalled {
      private val slot1 = slot<List<Patient>>()
      private val slot2 = slot<List<Drug>>()

      init {
        every { simulator.simulate(capture(slot1), capture(slot2)) } returnsArgument 0 //<- just bounce, not testing simulation
        application.run("D,D", "")
      }

      @Test
      fun simulationShouldBeCalled() {
        verify { simulator.simulate(any(), any()) }
      }

      @Test
      fun patientsArgumentShouldBeProper() {
        slot1.captured shouldNot beNull()
        slot1.captured.size shouldBe 2
        slot1.captured[0].currentState shouldBe State.DIABETES
        slot1.captured[1].currentState shouldBe State.DIABETES
      }

      @Test
      fun drugsArgumentShouldBeEmptyList() {
        slot2.captured shouldNot beNull()
        slot2.captured.size shouldBe 0
      }
    }
  }

  @Nested
  inner class GivenBothArguments {

    @Nested
    inner class WhenCalled {
      private val slot1 = slot<List<Patient>>()
      private val slot2 = slot<List<Drug>>()

      init {
        every { simulator.simulate(capture(slot1), capture(slot2)) } returnsArgument 0 //<- just bounce, not testing simulation
        application.run("D,D", "I")
      }

      @Test
      fun simulationShouldBeCalled() {
        verify { simulator.simulate(any(), any()) }
      }

      @Test
      fun patientsArgumentShouldBeProper() {
        slot1.captured shouldNot beNull()
        slot1.captured.size shouldBe 2
        slot1.captured[0].currentState shouldBe State.DIABETES
        slot1.captured[1].currentState shouldBe State.DIABETES
      }

      @Test
      fun drugsArgumentShouldBeProper() {
        slot2.captured shouldNot beNull()
        slot2.captured.size shouldBe 1
        slot2.captured[0] shouldBe Insulin
      }
    }
  }
}