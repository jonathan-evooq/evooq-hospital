package com.evooq.hospital.model

import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class AntibioticTest {

  @Nested
  inner class GivenYouAreHavingTuberculosis {
    val base = Patient(State.TUBERCULOSIS)

    @Nested
    inner class AndYouHaveNotHadInsulin {
      private val patient = base.copy(history = mutableListOf())

      @Nested
      inner class WhenYouTakeAntibiotic {
        private val actual = Antibiotic.applyTo(patient)

        @Test
        fun youShouldBeHealthy() {
          actual.currentState shouldBe State.HEALTHY
        }

        @Test
        fun yourDrugHistoryShouldContainAntibiotic() {
          actual.history shouldContain Antibiotic
        }
      }
    }

    @Nested
    inner class AndYouHaveHadInsulin {
      private val patient = base.copy(history = base.history + Insulin)

      @Nested
      inner class WhenYouTakeAntibiotic {
        private val actual = Antibiotic.applyTo(patient)

        @Test
        fun youShouldBeHavingFever() {
          actual.currentState shouldBe State.FEVER
        }

        @Test
        fun yourDrugHistoryShouldContainAntibiotic() {
          actual.history shouldContain Antibiotic
        }
      }
    }
  }

  @Nested
  inner class GivenYouAreNotHavingTuberculosis {
    val base = Patient(State.HEALTHY)

    @Nested
    inner class AndYouHaveNotHadInsulin {
      private val patient = base.copy(history = mutableListOf())

      @Nested
      inner class WhenYouTakeAntibiotic {
        private val actual = Antibiotic.applyTo(patient)

        @Test
        fun youShouldStayTheSame() {
          actual.currentState shouldBe actual.initialState
        }

        @Test
        fun yourDrugHistoryShouldContainAntibiotic() {
          actual.history shouldContain Antibiotic
        }
      }
    }

    @Nested
    inner class AndYouHaveHadInsulin {
      private val patient = base.copy(history = base.history + Insulin)

      @Nested
      inner class WhenYouTakeAntibiotic {
        private val actual = Antibiotic.applyTo(patient)

        @Test
        fun youShouldBeHavingFever() {
          actual.currentState shouldBe State.FEVER
        }

        @Test
        fun yourDrugHistoryShouldContainAntibiotic() {
          actual.history shouldContain Antibiotic
        }
      }
    }
  }
}