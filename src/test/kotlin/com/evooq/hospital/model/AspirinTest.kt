package com.evooq.hospital.model

import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class AspirinTest {

  @Nested
  inner class GivenYouAreHavingFever {
    val base = Patient(State.FEVER)

    @Nested
    inner class AndYouHaveNotHadParacetamol {
      private val patient = base.copy(history = mutableListOf())

      @Nested
      inner class WhenYouTakeAspirin {
        private val actual = Aspirin.applyTo(patient)

        @Test
        fun youShouldBeHealthy() {
          actual.currentState shouldBe State.HEALTHY
        }

        @Test
        fun yourDrugHistoryShouldContainAspirin() {
          actual.history shouldContain Aspirin
        }
      }
    }

    @Nested
    inner class AndYouHaveHadParacetamol {
      private val patient = base.copy(history = base.history + Paracetamol)

      @Nested
      inner class WhenYouTakeAspirin {
        private val actual = Aspirin.applyTo(patient)

        @Test
        fun youShouldBeDead() {
          actual.currentState shouldBe State.DEAD
        }

        @Test
        fun yourDrugHistoryShouldContainAspirin() {
          actual.history shouldContain Aspirin
        }
      }
    }
  }

  @Nested
  inner class GivenYouAreNotHavingFever {
    val base = Patient(State.DIABETES)

    @Nested
    inner class AndYouHaveNotHadParacetamol {
      private val patient = base.copy(history = mutableListOf())

      @Nested
      inner class WhenYouTakeAspirin {
        private val actual = Aspirin.applyTo(patient)

        @Test
        fun youShouldStayTheSame() {
          actual.currentState shouldBe actual.initialState
        }

        @Test
        fun yourDrugHistoryShouldContainAspirin() {
          actual.history shouldContain Aspirin
        }
      }
    }

    @Nested
    inner class AndYouHaveHadParacetamol {
      private val patient = base.copy(history = base.history + Paracetamol)

      @Nested
      inner class WhenYouTakeAspirin {
        private val actual = Aspirin.applyTo(patient)

        @Test
        fun youShouldBeDead() {
          actual.currentState shouldBe State.DEAD
        }

        @Test
        fun yourDrugHistoryShouldContainAspirin() {
          actual.history shouldContain Aspirin
        }
      }
    }
  }
}