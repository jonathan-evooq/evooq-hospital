package com.evooq.hospital.model

import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.random.nextInt

internal class MiracleTest {

  @Nested
  inner class GivenYouAreDead {
    private val patient = Patient(State.DEAD)

    @Nested
    inner class WhenMiracleHappens {
      init {
        mockkStatic("kotlin.random.RandomKt")
        every { Random.nextInt(1..1_000_000) } returns 1
      }

      private val actual = Miracle.applyTo(patient)

      @Test
      fun youShouldBeHealthy() {
        actual.currentState shouldBe State.HEALTHY
      }

      @Test
      fun yourDrugHistoryShouldContainMiracle() {
        actual.history shouldContain Miracle
      }
    }

    @Nested
    inner class WhenMiracleDoesNotHappen {
      init {
        mockkStatic("kotlin.random.RandomKt")
        every { Random.nextInt(1..1_000_000) } returns 2
      }

      private val actual = Miracle.applyTo(patient)

      @Test
      fun youShouldStayDead() {
        actual.currentState shouldBe State.DEAD
      }

      @Test
      fun yourDrugHistoryShouldNotContainMiracle() {
        actual.history shouldNotContain Miracle
      }
    }
  }

  @Nested
  inner class GivenYouAreNotDead {
    private val patient = Patient(State.FEVER)

    @Nested
    inner class WhenMiracleHappens {
      init {
        mockkStatic("kotlin.random.RandomKt")
        every { Random.nextInt(1..1_000_000) } returns 1
      }

      private val actual = Miracle.applyTo(patient)

      @Test
      fun youShouldStayTheSame() {
        actual.currentState shouldBe actual.initialState
      }

      @Test
      fun yourDrugHistoryShouldNotContainMiracle() {
        actual.history shouldNotContain Miracle
      }
    }

    @Nested
    inner class WhenMiracleDoesNotHappen {
      init {
        mockkStatic("kotlin.random.RandomKt")
        every { Random.nextInt(1..1_000_000) } returns 2
      }

      private val actual = Miracle.applyTo(patient)

      @Test
      fun youShouldStayTheSame() {
        actual.currentState shouldBe actual.initialState
      }

      @Test
      fun yourDrugHistoryShouldNotContainMiracle() {
        actual.history shouldNotContain Miracle
      }
    }
  }
}