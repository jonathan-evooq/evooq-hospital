package com.evooq.hospital.model

import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class ParacetamolTest {

  @Nested
  inner class GivenYouAreHavingFever {
    val base = Patient(State.FEVER)

    @Nested
    inner class AndYouHaveNotHadAspirin {
      private val patient = base.copy(history = mutableListOf())

      @Nested
      inner class WhenYouTakeParacetamol {
        private val actual = Paracetamol.applyTo(patient)

        @Test
        fun youShouldBeHealthy() {
          actual.currentState shouldBe State.HEALTHY
        }

        @Test
        fun yourDrugHistoryShouldContainParacetamol() {
          actual.history shouldContain Paracetamol
        }
      }
    }

    @Nested
    inner class AndYouHaveHadAspirin {
      private val patient = base.copy(history = base.history + Aspirin)

      @Nested
      inner class WhenYouTakeParacetamol {
        private val actual = Paracetamol.applyTo(patient)

        @Test
        fun youShouldBeDead() {
          actual.currentState shouldBe State.DEAD
        }

        @Test
        fun yourDrugHistoryShouldContainParacetamol() {
          actual.history shouldContain Paracetamol
        }
      }
    }
  }

  @Nested
  inner class GivenYouAreNotHavingFever {
    val base = Patient(State.DIABETES)

    @Nested
    inner class AndYouHaveNotHadAspirin {
      private val patient = base.copy(history = mutableListOf())

      @Nested
      inner class WhenYouTakeParacetamol {
        private val actual = Paracetamol.applyTo(patient)

        @Test
        fun youShouldStayTheSame() {
          actual.currentState shouldBe actual.initialState
        }

        @Test
        fun yourDrugHistoryShouldContainParacetamol() {
          actual.history shouldContain Paracetamol
        }
      }
    }

    @Nested
    inner class AndYouHaveHadAspirin {
      private val patient = base.copy(history = base.history + Aspirin)

      @Nested
      inner class WhenYouTakeParacetamol {
        private val actual = Paracetamol.applyTo(patient)

        @Test
        fun youShouldBeDead() {
          actual.currentState shouldBe State.DEAD
        }

        @Test
        fun yourDrugHistoryShouldContainParacetamol() {
          actual.history shouldContain Paracetamol
        }
      }
    }
  }
}