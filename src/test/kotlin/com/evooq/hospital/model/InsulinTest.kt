package com.evooq.hospital.model

import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class InsulinTest {

  @Nested
  inner class GivenYouAreHavingDiabetes {
    val base1 = Patient(State.DIABETES)

    @Nested
    inner class AndYouHaveNotHadAntibiotic {
      val base2 = base1.copy(history = mutableListOf())

      @Nested
      inner class AndYouAreAlreadyDead {
        private val patient = base2.copy(state = State.DEAD)

        @Nested
        inner class WhenYouTakeInsulin {
          private val actual = Insulin.applyTo(patient)

          @Test
          fun youShouldRaiseFromTheDead() {
            actual.currentState shouldNotBe State.DEAD // <- necromancy!
          }

          @Test
          fun yourDrugHistoryShouldContainInsulin() {
            actual.history shouldContain Insulin
          }
        }
      }

      @Nested
      inner class AndYouAreNotDead {
        private val patient = base2

        @Nested
        inner class WhenYouTakeInsulin {
          private val actual = Insulin.applyTo(patient)

          @Test
          fun youShouldNotDie() {
            actual.currentState shouldNotBe State.DEAD
          }

          @Test
          fun yourDrugHistoryShouldContainInsulin() {
            actual.history shouldContain Insulin
          }

        }
      }
    }

    @Nested
    inner class AndYouHaveHadAntibiotic {
      val base2 = base1.copy(history = base1.history + Antibiotic)

      @Nested
      inner class AndYouAreAlreadyDead {
        private val patient = base2.copy(state = State.DEAD)

        @Nested
        inner class WhenYouTakeInsulin {
          private val actual = Insulin.applyTo(patient)

          @Test
          fun youShouldRaiseFromTheDead() {
            actual.currentState shouldNotBe State.DEAD // <- necromancy!
          }

          @Test
          fun youShouldStillBeHavingDiabetes() {
            actual.currentState shouldBe State.DIABETES
          }

          @Test
          fun yourDrugHistoryShouldContainInsulin() {
            actual.history shouldContain Insulin
          }
        }
      }

      @Nested
      inner class AndYouAreNotDead {
        private val patient = base2

        @Nested
        inner class WhenYouTakeInsulin {
          private val actual = Insulin.applyTo(patient)

          @Test
          fun youShouldNotDie() {
            actual.currentState shouldNotBe State.DEAD
          }

          @Test
          fun youShouldStillBeHavingDiabetes() {
            actual.currentState shouldBe State.DIABETES
          }

          @Test
          fun yourDrugHistoryShouldContainInsulin() {
            actual.history shouldContain Insulin
          }
        }
      }
    }
  }

  @Nested
  inner class GivenYouAreHealthy {
    val base1 = Patient(State.HEALTHY)

    @Nested
    inner class AndYouHaveNotHadAntibiotic {
      private val patient = base1.copy(history = mutableListOf())

      @Nested
      inner class WhenYouTakeInsulin {
        private val actual = Insulin.applyTo(patient)

        @Test
        fun youShouldStayTheSame() {
          actual.currentState shouldBe actual.initialState
        }

        @Test
        fun yourDrugHistoryShouldContainInsulin() {
          actual.history shouldContain Insulin
        }
      }
    }

    @Nested
    inner class AndYouHaveHadAntibiotic {
      private val patient = base1.copy(history = base1.history + Antibiotic)

      @Nested
      inner class WhenYouTakeInsulin {
        private val actual = Insulin.applyTo(patient)

        @Test
        fun youShouldBeHavingFever() {
          actual.currentState shouldBe State.FEVER
        }

        @Test
        fun yourDrugHistoryShouldContainInsulin() {
          actual.history shouldContain Insulin
        }
      }
    }
  }
}