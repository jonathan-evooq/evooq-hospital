package com.evooq.hospital.model

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class DrugTest {

  @TestFactory
  fun shouldRecognizeDrugFromShorthand(): List<DynamicTest> {
    return listOf(
      "As" to Aspirin,
      "An" to Antibiotic,
      "I"  to Insulin,
      "P"  to Paracetamol
    )
      .mapIndexed { index, (argument, expected) ->
        dynamicTest("[${index + 1}] $argument -> $expected") {
          Drug.of(argument) shouldBe expected
        }
      }
  }

  @ParameterizedTest
  @ValueSource(strings = ["something", "some other thing", "another thing", "more things"])
  fun shouldThrowExceptionOnUnrecognizedShorthand(input: String) {
    assertThrows<IllegalArgumentException> { Drug.of(input) }
  }
}