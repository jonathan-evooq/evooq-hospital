package com.evooq.hospital.service

import com.evooq.hospital.model.*
import io.kotest.matchers.collections.beEmpty
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainInOrder
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class SimulatorTest {

  @TestFactory
  fun shouldSimulateForSinglePatientAndSingleDrug(): List<DynamicTest> {
    data class TestCase(
      val patient: State,
      val drug: Drug,
      val result: State,
    )

    val cases = listOf(
      TestCase(State.FEVER, Aspirin, State.HEALTHY),
      TestCase(State.FEVER, Paracetamol, State.HEALTHY),
      TestCase(State.DIABETES, Insulin, State.DIABETES),
      TestCase(State.TUBERCULOSIS, Antibiotic, State.HEALTHY),
    )

    return cases
      .mapIndexed { index, (initial, drug, expectation) ->
        dynamicTest("$index. [$initial]+[$drug] -> [$expectation]") {
          val result = Simulator().simulate(listOf(Patient(initial)), listOf(drug))

          result shouldNot beEmpty()
          result.size shouldBe 1
          result[0].currentState shouldBe expectation
          result[0].history shouldContain drug
        }
      }
  }

  @TestFactory
  fun shouldSimulateForSinglePatientAndMultiDrugs(): List<DynamicTest> {
    data class TestCase(
      val patient: State,
      val drugs: List<Drug>,
      val result: State,
    )

    val cases = listOf(
      TestCase(State.FEVER, listOf(Aspirin, Paracetamol), State.DEAD),
      TestCase(State.FEVER, listOf(Paracetamol, Aspirin), State.DEAD),
      TestCase(State.TUBERCULOSIS, listOf(Antibiotic, Insulin), State.FEVER),
      TestCase(State.TUBERCULOSIS, listOf(Insulin, Antibiotic), State.FEVER),
    )

    return cases
      .mapIndexed { index, (initial, drugs, expectation) ->
        dynamicTest("$index. [$initial]+$drugs -> [$expectation]") {
          val result = Simulator().simulate(listOf(Patient(initial)), drugs)

          result shouldNot beEmpty()
          result.size shouldBe 1
          result[0].currentState shouldBe expectation
          result[0].history shouldContainInOrder drugs
        }
      }
  }

  @TestFactory
  fun shouldSimulateForMultiPatientsAndMultiDrugs(): List<DynamicTest> {
    data class TestCase(
      val patients: List<State>,
      val drugs: List<Drug>,
      val result: List<State>,
    )

    val cases = listOf(
      TestCase(listOf(State.DIABETES, State.FEVER, State.FEVER), listOf(Aspirin, Insulin), listOf(State.DIABETES, State.HEALTHY, State.HEALTHY)),
      TestCase(listOf(State.DIABETES, State.FEVER, State.FEVER), listOf(Aspirin, Antibiotic), listOf(State.DEAD, State.HEALTHY, State.HEALTHY)), // <- diabetic without insulin will die!
      TestCase(listOf(State.FEVER, State.TUBERCULOSIS, State.DIABETES), listOf(Aspirin, Antibiotic, Insulin), listOf(State.FEVER, State.FEVER, State.DIABETES)),
    )

    return cases
      .mapIndexed { index, (initials, drugs, expectations) ->
        dynamicTest("$index. $initials+$drugs -> $expectations") {
          val results = Simulator().simulate(initials.map { Patient((it)) }, drugs)

          results shouldNot beEmpty()
          results.size shouldBe initials.size

          results.zip(expectations).forEach { (actual, expectation) ->
            actual.currentState shouldBe expectation
            actual.history shouldContainInOrder drugs
          }
        }
      }
  }
}