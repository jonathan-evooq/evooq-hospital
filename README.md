# Hospital Simulator

This hospital simulator is implemented as a semi Finite-State-Machine. There are some library already available 
specifically for Kotlin that enables FSM's state management, this application tries to avoid any similarities.

## Getting Started

### Problem Statement

Read [here][1]

### Pre-requisite

To run the application you will need:
* Kotlin 1.4+ or higher
* Java 13+ or higher
* Maven 3.3 or higher

### Setup

Execute:
```bash
$ mvn clean package
```

That's all!

## Running the Application

```bash
$ java target/evooq-hospital-jar-with-dependencies.jar [patients] [drugs]
```

The application takes in 2 arguments:
* `[patients]`: Initial states of the patients in comma-separated list, e.g.: `D,D,F`
* `[drugs]`: Drugs to be administered to all patients in comma-separated list, e.g.: `As,I`

Example:

```bash
$ java target/evooq-hospital-jar-with-dependencies.jar D,D,F As,I
```

This creates a simulation of 3 patients with initial states as `Diabetes`, `Diabetes`, and `Fever`. All 3 patients will 
be administered 2 drugs which are `Aspirin` and `Insulin`. The application will return the final tally of patients 
based on their states.

## Assumptions & Notes
* [Kotlin][2], because it's more succinct than Java, although it generates larger Jar file.
* Kotlin encourages [multiple class declarations][3] in the same file as long as they are semantically related.
* This implementation tries to avoid any similarities with [Tinder's StateMachine][4].
* Tests and test organizations may have been too much, but [this][5] is how JUnit5 is prescribed to be tested.

[1]: CHALLENGE.md
[2]: https://kotlinlang.org
[3]: https://kotlinlang.org/docs/reference/coding-conventions.html#source-file-organization
[4]: https://github.com/Tinder/StateMachine
[5]: https://junit.org/junit5/docs/current/user-guide/#writing-tests-nested
